
const FIRST_NAME = "Ioana-Mihaela";
const LAST_NAME = "Mitrache";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function numberParser(value) {

    if(value == Infinity || value == -Infinity) return NaN;
    else if(isNaN(value)) return NaN;
    else if(value>=Number.MAX_VALUE || value<=Number.MIN_VALUE) return NaN;
    else return Math.floor(value);

}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

